#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function

import os.path
import warnings
import sys

try:
    from setuptools import setup, Command
    setuptools_available = True
except ImportError:
    from distutils.core import setup, Command
    setuptools_available = False
from distutils.spawn import spawn

# Get the version from youtube_dl/version.py without importing the package
exec(compile(open('youtube_dl/version.py').read(),
             'youtube_dl/version.py', 'exec'))

DESCRIPTION = 'YouTube video downloader'
LONG_DESCRIPTION = 'Command-line program to download videos from YouTube.com and other video sites'

files_spec = [
    ('etc/bash_completion.d', ['hypervideo.bash-completion']),
    ('etc/fish/completions', ['hypervideo.fish']),
    ('share/doc/youtube_dl', ['README.txt']),
    ('share/man/man1', ['hypervideo.1'])
]

root = os.path.dirname(os.path.abspath(__file__))
data_files = []
for dirname, files in files_spec:
    resfiles = []
    for fn in files:
        if not os.path.exists(fn):
            warnings.warn('Skipping file %s since it is not present. Type  make  to build all automatically generated files.' % fn)
        else:
            resfiles.append(fn)
    data_files.append((dirname, resfiles))

params = {
    'data_files': data_files,
}

if setuptools_available:
    params['entry_points'] = {'console_scripts': ['hypervideo = youtube_dl:main']}
else:
    params['scripts'] = ['bin/hypervideo']


class build_lazy_extractors(Command):
    description = 'Build the extractor lazy loading module'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        spawn(
            [sys.executable, 'devscripts/make_lazy_extractors.py', 'youtube_dl/extractor/lazy_extractors.py'],
            dry_run=self.dry_run,
        )


setup(
    name='youtube_dl',
    version=__version__,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    url='https://libregit.org/heckyel/hypervideo',
    author='Hyperbola Project',
    author_email='hyperbola@hyperbola.info',
    maintainer='Jesús E.',
    maintainer_email='heckyel@hyperbola.info',
    license='CC0-1.0',
    packages=[
        'youtube_dl',
        'youtube_dl.extractor', 'youtube_dl.downloader',
        'youtube_dl.postprocessor'],

    # Provokes warning on most systems (why?!)
    # test_suite = 'nose.collector',
    # test_requires = ['nosetest'],

    classifiers=[
        'Topic :: Multimedia :: Video',
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'License :: Public Domain',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: IronPython',
        'Programming Language :: Python :: Implementation :: Jython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    cmdclass={'build_lazy_extractors': build_lazy_extractors},
    **params
)
